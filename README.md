# Bonus Date Printer

### About the app

**Bonus Date Printer** tiny app has only one magic method that can check all dates between two given years and print date if it remains the same if numbers of date are reversed.  
 App can check and print dates that satisfy date format *YYYY-MM-DD*, ranging from 0000-01-01 to 9999-12-31.

### Initial task:

Implement a method that prints all dates between two given years that remain the same if numbers of the date are reversed. 

Method signature:<br/>
**static void printBonusDatesBetween(int fromYear, int toYear)**

It should print dates in interval from fromYear (inclusive) to toYear (exclusive) that satisfy the condition.

For example, calling printBonusDatesBetween(2010, 2015) should print:<br/>
`2010-01-02`<br/>
`2011-11-02`

### Demo

![](demo.png)

### How to run
1. Clone / download project and open in your IDE
2. In *main* method of *Main.java* class specify arguments you want to pass to the *printBonusDatesBetween* method parameters *fromYear* and *toYear*
2. Run *Main.java*

### Tech used
*Java 11, Apache Maven*

