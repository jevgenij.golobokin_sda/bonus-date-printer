package com.eugenegolobokin.app;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {

        printBonusDatesBetween(2010, 2015);

    }

    static void printBonusDatesBetween(int fromYear, int toYear) {

        LocalDate startDate = LocalDate.of(fromYear, 1, 1);
        LocalDate endDate = LocalDate.of(toYear, 1, 1).minusDays(1);

        LocalDate currentDateToCheck = startDate;

        while (currentDateToCheck.isBefore(endDate)) {
            String currentDate = currentDateToCheck.toString().replaceAll("-", "");
            String reversedDate = new StringBuilder(currentDate).reverse().toString();

            if (currentDate.equals(reversedDate)) {
                System.out.println(currentDateToCheck);
            }

            currentDateToCheck = currentDateToCheck.plusDays(1);
        }
    }
}
